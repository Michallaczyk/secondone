package football.repositories.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import football.entities.Player;

public class PlayerRepository extends AbstractRepository<Player> {

	public PlayerRepository() {
		super(PlayerRepository.class);
	}

	@Override
	protected String getAddCallSql() {
		return "{CALL dodaj_pilkarza(?,?,?,?,?,?,?)}";
	}

	@Override
	protected int getAddCallOutParamIndex() {
		return 7;
	}

	@Override
	protected String getDeleteCallSql() {
		return "{CALL usun_pilkarza(?)}";
	}

	@Override
	protected String getGetPrepStmtSql() {
		return "SELECT * " + 
				"FROM pilkarz " +
				"WHERE id_pilkarz = ?";
	}

	@Override
	protected void setAddCallableStatementParams(Player player) throws SQLException {
		addCallableStatement.setString(1, player.getFirstName());
		addCallableStatement.setString(2, player.getLastName());
		addCallableStatement.setDate(3, 
				new java.sql.Date(player.getDateOfBirth().getTime()));
		addCallableStatement.setInt(4, player.getSpeed());
		addCallableStatement.setInt(5, player.getSpeed());
		addCallableStatement.setInt(6, player.getTeamId());
	}

	@Override
	protected Player getEntityFromResultSet(ResultSet resultSet) throws SQLException {
		return new Player(
				resultSet.getInt("id_pilkarz"),
				resultSet.getString("imie"),
				resultSet.getString("nazwisko"),
				new Date(resultSet.getDate("data_urodzenia").getTime()),
				resultSet.getInt("szybkosc"),
				resultSet.getInt("id_aktualnej_druzyny")
			);
	}
}
